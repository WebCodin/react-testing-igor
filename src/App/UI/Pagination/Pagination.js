import React from 'react';
import { Button } from '@material-ui/core';
import styled from 'styled-components';

const ButtonWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 15px 0;
    margin: 0 0 30px;
`;

const StyledButton = styled(Button)`
    && {
        margin: 0 3px;
        min-width: 160px;

        @media (max-width: 767px) {
            min-width: 110px;
        }
    }
`;


const Pagination = (props) => {

    return (
        <ButtonWrapper>
            <StyledButton variant="outlined" onClick={() => props.click(-1)} disabled={props.active === 1 }>Previous</StyledButton>
            <div>
                Page {props.active}/{props.length}
            </div>
            <StyledButton variant="outlined" onClick={() => props.click(1)} disabled={props.length === props.active}>Next</StyledButton>
        </ButtonWrapper>
    );
};

export default Pagination;
