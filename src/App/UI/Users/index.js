/* @flow */

import * as React from 'react';
import Search from './Search';
import List from './List';

export default () => (
    <section>
        <Search />
        <List />
    </section>
);
