/* @flow */

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'react-redux';
import UsersListView from './UsersListView';
import fetchUsers from './../../../Application/fetchUsers';
import FetchUsers from './../../../Application/Query/FetchUsers';
import ListItem from './ListItem/ListItem';
import List from '@material-ui/core/List';
import styled from 'styled-components';
import Pagination from '../../Pagination/Pagination';
import InfiniteScroll from 'react-infinite-scroller';


type Props = {|
    dispatch: Dispatch,
    users: ?UsersListView,
|};

const Wrapper = styled.div`
    max-width: 750px;
    margin: 0 auto;
`;

const EmptyUsers = styled.p`
    text-align: center;
`;

export class UsersList extends React.Component<Props> {
    state = {
        currentPage: 1,
        itemsOnPage: 3,
        pagination: false,
        lazyLoadingCounter: 2,
        laziLoadItems: [],
        isLoading: true
    }

    changePage = (pageNumber) => {
        const newPage = this.state.currentPage + pageNumber;
        this.setState({
            currentPage: newPage
        }, () => {
            this.props.getUsers(newPage, this.state.itemsOnPage);
        });
    }

    componentWillReceiveProps(props) {
        this.setState(state => ({
            laziLoadItems: state.laziLoadItems.concat(props.users.pages.get(this.state.currentPage))
        }));

    }

    loadMore = () => {
        this.setState(state => ({
            currentPage: state.currentPage + 1
        }), () => {
            this.props.getUsers(this.state.currentPage, this.state.itemsOnPage);
        });
    }

    componentWillMount = () => {
        this.props.getUsers(this.state.currentPage, this.state.itemsOnPage);
    }

    render() {
        let usersList = null;
        let length = null;
        let pagination = null;

        if (this.props.users && this.props.users.pages.get(this.state.currentPage)) {
            let currentPgeUsers;
            if (this.state.pagination) {
                currentPgeUsers = this.props.users.pages.get(this.state.currentPage);
            } else {
                currentPgeUsers = this.state.laziLoadItems;
            }

            length = this.props.users.pages.size;
            usersList = currentPgeUsers.map((user) => {
                return <ListItem key={user.id} user={user} />;
            });
            if (length > 1 && this.state.pagination) {
                pagination = <Pagination click={this.changePage} active={this.state.currentPage} length={length} />;
            }
            if (!this.state.pagination) {
                usersList = <InfiniteScroll
                    threshold={50}
                    loadMore={this.loadMore}
                    hasMore={length > this.state.currentPage}>
                    {usersList}
                </InfiniteScroll>;
            }
        } else {
            usersList = <EmptyUsers>No users found</EmptyUsers>;
        }

        return <Wrapper>
            <List>{usersList}</List>
            {pagination}
        </Wrapper>;
    }
}

const mapDispatchToProps = ( dispatch ) => {
    return {
        getUsers: (page, perPage) => dispatch(fetchUsers(new FetchUsers(page: number, perPage: number)))
    };
};

const mapStateToProps = ({ usersListReducers }) => {
    return { ...usersListReducers };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
