import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { UsersList } from './index';
import Pagination from './../../Pagination/Pagination';
import mockStore from '../../../../Testing/mockStore';
import ListItem from './ListItem/ListItem';

configure({adapter: new Adapter()});

describe('<List />', () => {
    it('Do not display pagination if there are less than two pages', () => {
        const users = () => {
            let pages = new Map();

            // creates data with one page
            for (let index = 0; index < 1; index++) {
                pages.set(index, [
                    {id: index + '-' + 1},
                    {id: index + '-' + 2},
                    {id: index + '-' + 3},
                    {id: index + '-' + 4},
                    {id: index + '-' + 5},
                    {id: index + '-' + 6}
                ]);
            }

            return {
                pages: pages
            };
        };
        const wrapper = shallow(<UsersList users={users()} getUsers={() => {}} />);
        expect(wrapper.find(Pagination)).not.toHaveLength(1);
    });

    it('Display pagination if there are more than one page', () => {
        const users = () => {
            let pages = new Map();

            // creates data with two pages
            for (let index = 0; index < 2; index++) {
                pages.set(index, [
                    {id: index + '-' + 1},
                    {id: index + '-' + 2},
                    {id: index + '-' + 3},
                    {id: index + '-' + 4},
                    {id: index + '-' + 5},
                    {id: index + '-' + 6}
                ]);
            }

            return {
                pages: pages
            };
        };
        const wrapper = shallow(<UsersList users={users()} getUsers={() => {}} />);
        wrapper.setState({
            pagination: true
        });
        expect(wrapper.find(Pagination)).toHaveLength(1);
    });

    it('If there are no users to display, do not display items', () => {
        const wrapper = shallow(<UsersList users={null} getUsers={() => {}} />);
        expect(wrapper.find(ListItem)).toHaveLength(0);
    });
});
