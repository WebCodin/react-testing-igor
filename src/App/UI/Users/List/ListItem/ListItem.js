import * as React from 'react';
import { UserPayload } from '../../../../Application/Action/usersFetched';
import ListItemM from '@material-ui/core/ListItem';
import { Avatar, ListItemText, Button } from '@material-ui/core';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import styled from 'styled-components';

type Props = {|
  user: UserPayload
|};

const StyledListItem = styled(ListItemM)`
    && {
        margin-bottom: 30px;
        border: 1px solid #efefef;
        height: 150px;
        align-items: stretch;

        @media (max-width: 767px) {
            height: 100px;
            align-items: flex-start;
            padding: 10px;
        }
    }
`;

const StyledListItemSecondaryAction = styled(ListItemSecondaryAction)`
    && {
        right: 10px;
        display: flex;
        flex-direction: column;

        @media (max-width: 767px) {
            flex-direction: row;
            top: auto;
            bottom: 13px;
            left: 97px;
            transform: none;
        }
    }
`;

const StyledButton = styled(Button)`
    && {
        margin: 10px 0;

        @media (max-width: 767px) {
            margin: 0 5px;
            min-height: 25px;
            padding: 2px 10px;
        }
    }
`;

const StyledListItemText = styled(ListItemText)`
    && {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        padding: 20px 114px 20px 48px;
        position: relative;
        box-sizing: border-box;
        height: 100%;

        @media (max-width: 767px) {
            padding: 0 15px;
            height: auto;
        }
    }

    &::before,
    &::after {
        content: '';
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        height: 150px;
        width: 1px;
        background: #efefef;

         @media (max-width: 767px) {
            display: none;
         }
    }

    &::before {
        left: 24px;
    }

    &::after {
        right: 90px;
    }
`;

const FirstName = styled.span`
    text-transform: capitalize;
    display: block;

    @media (max-width: 767px) {
        display: inline;
    }
`;

const StyledAvatar = styled(Avatar)`
    && {
        height: 128px;
        width: 128px;

        @media (max-width: 767px) {
            width: 76px;
            height: 76px;
        }
    }
`;

const LastName = styled.span`
    text-transform: uppercase;
`;

class ListItem extends React.Component<Props> {

    render() {
        let name = <div>
            <LastName>{this.props.user.lastname} </LastName>
            <FirstName>{this.props.user.firstname}</FirstName>
        </div>;

        return (
            <StyledListItem >
                <StyledAvatar>
                    <img src={this.props.user.picture} alt={this.props.user.firstname + ' ' + this.props.user.lastname} />
                </StyledAvatar>
                <StyledListItemText primary={name} secondary={this.props.user.email} />
                <StyledListItemSecondaryAction>
                    <StyledButton variant="outlined" color="primary">
                        Edit
                    </StyledButton>
                    <StyledButton variant="outlined" color="secondary">
                        Delete
                    </StyledButton>
                </StyledListItemSecondaryAction>
            </StyledListItem>
        );
    }
}

export default ListItem;
