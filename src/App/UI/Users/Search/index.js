/* @flow */

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'react-redux';
import UserView from './UserView';
import Search from '@material-ui/icons/Search';
import styled from 'styled-components';
import filterUsers from './../../../Application/filterUsers';
import FilterUsers from './../../../Application/Query/FilterUsers';
import { TextField } from '@material-ui/core';
import SearchDrop from './SearchDrop/SearchDrop';

type Props = {|
    dispatch: Dispatch,
    users: ?Array<UserView>,
|};

const SearchWrapper = styled.div`
    border: 1px solid #efefef;
    max-width: 750px;
    margin: 30px auto;
    height: 100px;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 20px;
    box-sizing: border-box;
`;

const Icon = styled.div`
    display: inline-block;
    margin-right: 5px;
`;

const StyledTextField = styled(TextField)`
    && {
        margin: 0;
    }
`;

const FormWrapper = styled.div`
    position: relative;
    display: flex;
    align-items: center;
`;

const SearchDropWrapper = styled.div`
    position: relative;
`;

export class UsersSearch extends React.Component<Props> {
    state = {
        value: '',
        maxResults: 5,
        showDrop: true
    }

    changeHandler = (e) => {
        const value = e.target.value;
        let maxResults = this.state.maxResults;
        if (!value) {
            maxResults = 0;
        }
        this.setState({
            value: value
        }, () => {
            this.props.filter(value, maxResults);
        });
    }

    disableSearchDrop = () => {
        this.setState({
            showDrop: false
        });
    }

    enableSearchDrop = () => {
        this.setState({
            showDrop: true
        });
    }


    render() {
        let searchResults = null;
        if (this.props.users && this.props.users.length && this.state.showDrop) {
            searchResults = <SearchDrop users={this.props.users} />;
        }

        return (
            <SearchWrapper>
                <FormWrapper>
                    <Icon>
                        <Search />
                    </Icon>
                    <SearchDropWrapper>
                        <StyledTextField
                            value={this.state.value}
                            margin="normal"
                            onFocus={this.enableSearchDrop}
                            onBlur={this.disableSearchDrop}
                            onChange={(e) => this.changeHandler(e)} />
                        {searchResults}
                    </SearchDropWrapper>
                </FormWrapper>
            </SearchWrapper>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        filter: (filter, maxResults) => dispatch(filterUsers(new FilterUsers(filter: string, maxResults: number)))
    };
};

const mapStateToProps = ({ usersSearchReducers }) => {
    return { ...usersSearchReducers };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersSearch);
