import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { UsersSearch } from './index';
import SearchDrop from './SearchDrop/SearchDrop';

configure({adapter: new Adapter()});

describe('<Search />', () => {
    it('Do not display a dropdown if the showDrop is "false" ', () => {
        const wrapper = shallow(<UsersSearch users={[1, 2]} />);
        wrapper.setState({
            showDrop: false
        });
        expect(wrapper.find(SearchDrop)).toHaveLength(0);
    });

    it('Do not display a dropdown if there are no users to show', () => {
        const wrapper = shallow(<UsersSearch users={[]} />);
        wrapper.setState({
            showDrop: true
        });
        expect(wrapper.find(SearchDrop)).toHaveLength(0);
    });
});
