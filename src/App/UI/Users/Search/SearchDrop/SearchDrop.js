import React from 'react';
import styled from 'styled-components';
import { ListItem, List, ListItemText } from '@material-ui/core';

const StyledListItem = styled(ListItem)`
    cursor: pointer;

    &:hover {
        background: #ebebeb;
    }
`;

const StyledList = styled(List)`
    && {
        padding: 0;
    }
`;

const SearchDropdown = styled.div`
    position: absolute;
    top: 100%;
    z-index: 3;
    left: 0;
    right: 0;
    background: #fff;
    box-shadow: 0 0 3px rgba(0,0,0,.3);
`;

const SearchDrop = (props) => {
    return (
        <SearchDropdown>
            <StyledList>
                {props.users.map((user) => {
                    return <StyledListItem key={user.id}>
                        <ListItemText primary={user.firstname} secondary={user.lastname}/>
                    </StyledListItem>;
                })}
            </StyledList>
        </SearchDropdown>
    );
};

export default SearchDrop;
